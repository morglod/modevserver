'use strict';

module.exports = (
    class Collection {
        constructor() {
            this.save = this.save.bind(this);
            this.collection = require(require('path').join(__dirname, './collection_json.json'));
        }

        process(req, res, onNew) {
            const path = `${req.method} ${req.url}`;
            const item = this.collection.find(x => x.path === path);
            if(!item) {
                const reqInfo = {body: req.body, url: req.url, method: req.method};
                this.collection.push(Object.assign({path, answer: '[]'}, reqInfo));
                this.save();
                onNew(reqInfo, (answer => {
                    this.collection.find(x => x.path === path).answer = answer;
                    this.save();
                }));
                res.end();
            }
            else res.api(JSON.parse(item.answer));
        }

        save() {
            const fs = require('fs');
            const path = require('path');
            fs.writeFile(path.join(__dirname, './collection_json.json'), JSON.stringify(this.collection), 'utf8');
        }
    }
);
