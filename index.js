'use strict';

const WebServer = require('./webserver');

function appendItem({body, url, method, answer}, answerCallback) {
    const list = document.getElementById('list');

    const item = document.createElement('div');
    const br = document.createElement('br');
    const textarea = document.createElement('textarea');
    const bodyTextarea = document.createElement('textarea');
    const button = document.createElement('button');
    button.innerText = 'Set';
    bodyTextarea.innerText = (typeof body === 'object') ? JSON.stringify(body) : body;
    item.innerText = `Path: ${method} ${url}`;
    bodyTextarea.readonly = true;
    textarea.value = answer || '';
    item.appendChild(br);
    item.appendChild(bodyTextarea);
    item.appendChild(textarea);
    item.appendChild(button);
    list.appendChild(item);

    button.onclick = () => answerCallback(textarea.value);
}

function startServer() {
    const server_port = document.getElementById('server_port');
    const server = new WebServer();

    server.collection.collection.forEach(x => {
        const answerCallback = (answer => {
            server.collection.collection.find(x2 => x2.path === x.path).answer = answer;
            server.collection.save();
        });

        appendItem({body: x.body, url: x.url, method: x.method, answer: x.answer}, answerCallback);
    });

    server.run({
        port: server_port.value,
        onNewRequest: appendItem
    });
}
