'use strict';

const http = require('http');
const Collection = require('./collection');

module.exports = (
    class WebServer {
        constructor() {
            this._listener = this._listener.bind(this);
            this._newRequest = this._newRequest.bind(this);
            this.run = this.run.bind(this);
            this.stop = this.stop.bind(this);

            this.httpServer = http.createServer(this._listener);
            this.collection = new Collection();
        }

        _listener(req, res) {
            console.log(req, res);
              res.setHeader('Access-Control-Allow-Origin', '*');
              res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Location, WWW-Authenticate, Authorization');
              res.setHeader('Access-Control-Allow-Credentials', 'true');
              res.setHeader('Access-Control-Expose-Headers', 'Authorization, x-pixel-role');
              res.setHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,OPTION,DELETE');
              res.setHeader('x-pixel-role', 'admin');
              res.setHeader('Origin', '*');

              res.json = (o) => res.end(JSON.stringify(o));

              res.api = (result = [], errorCode = 0, errorMessage = '') => (
                  res.json({
                    errorCode,
                    errorMessage,
                    result: Array.isArray(result) && result || [ result ]
                  })
              );

              let body = '';
              req.on('data', data => {
                  console.log('data', data);
                  body += data;
              });
              req.on('end', () => {
                  try {
                      req.body = JSON.parse(body);
                  } catch(e) {
                      req.body = body;
                  }
                  this.collection.process(req, res, this._newRequest, body);
              });
        }

        _newRequest(req, res, answerCallback) {
            if(this._onNewRequest) this._onNewRequest(req, res, answerCallback);
        }

        run({port, host, then, onError, onNewRequest}) {
            this._onNewRequest = onNewRequest || ((path, _collection) => console.log(`new request path: ${path}`));

            this.httpServer.listen(port, host, (err) => {
                if(err) {
                    console.error('Failed start server on', port, host, 'with error', err);
                    if(onError) onError(err);
                } else if(then) then(this);
            });
        }

        stop() {
            this.httpServer.close();
        }
    }
);
